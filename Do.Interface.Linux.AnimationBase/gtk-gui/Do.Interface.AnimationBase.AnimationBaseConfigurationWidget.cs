
// This file has been generated by the GUI designer. Do not modify.
namespace Do.Interface.AnimationBase
{
	public partial class AnimationBaseConfigurationWidget
	{
		private global::Gtk.VBox vbox4;
		private global::Gtk.HBox hbox4;
		private global::Gtk.Label label8;
		private global::Gtk.ColorButton background_colorbutton;
		private global::Gtk.Button clear_background;
		private global::Gtk.CheckButton shadow_check;
		private global::Gtk.CheckButton animation_check;
        
		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget Do.Interface.AnimationBase.AnimationBaseConfigurationWidget
			global::Stetic.BinContainer.Attach (this);
			this.Name = "Do.Interface.AnimationBase.AnimationBaseConfigurationWidget";
			// Container child Do.Interface.AnimationBase.AnimationBaseConfigurationWidget.Gtk.Container+ContainerChild
			this.vbox4 = new global::Gtk.VBox ();
			this.vbox4.Name = "vbox4";
			this.vbox4.Spacing = 6;
			// Container child vbox4.Gtk.Box+BoxChild
			this.hbox4 = new global::Gtk.HBox ();
			this.hbox4.Name = "hbox4";
			this.hbox4.Spacing = 6;
			// Container child hbox4.Gtk.Box+BoxChild
			this.label8 = new global::Gtk.Label ();
			this.label8.Name = "label8";
			this.label8.Xalign = 0F;
			this.label8.LabelProp = global::Mono.Unix.Catalog.GetString ("Background Color:");
			this.hbox4.Add (this.label8);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.hbox4 [this.label8]));
			w1.Position = 0;
			w1.Expand = false;
			w1.Fill = false;
			// Container child hbox4.Gtk.Box+BoxChild
			this.background_colorbutton = new global::Gtk.ColorButton ();
			this.background_colorbutton.CanFocus = true;
			this.background_colorbutton.Events = ((global::Gdk.EventMask)(784));
			this.background_colorbutton.Name = "background_colorbutton";
			this.background_colorbutton.UseAlpha = true;
			this.background_colorbutton.Alpha = 65535;
			this.hbox4.Add (this.background_colorbutton);
			global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(this.hbox4 [this.background_colorbutton]));
			w2.Position = 1;
			w2.Expand = false;
			w2.Fill = false;
			// Container child hbox4.Gtk.Box+BoxChild
			this.clear_background = new global::Gtk.Button ();
			this.clear_background.CanFocus = true;
			this.clear_background.Name = "clear_background";
			this.clear_background.UseUnderline = true;
			this.clear_background.Relief = ((global::Gtk.ReliefStyle)(2));
			// Container child clear_background.Gtk.Container+ContainerChild
			global::Gtk.Alignment w3 = new global::Gtk.Alignment (0.5F, 0.5F, 0F, 0F);
			// Container child GtkAlignment.Gtk.Container+ContainerChild
			global::Gtk.HBox w4 = new global::Gtk.HBox ();
			w4.Spacing = 2;
			// Container child GtkHBox.Gtk.Container+ContainerChild
			global::Gtk.Image w5 = new global::Gtk.Image ();
			w5.Pixbuf = global::Stetic.IconLoader.LoadIcon (this, "gtk-clear", global::Gtk.IconSize.Menu);
			w4.Add (w5);
			// Container child GtkHBox.Gtk.Container+ContainerChild
			global::Gtk.Label w7 = new global::Gtk.Label ();
			w7.LabelProp = global::Mono.Unix.Catalog.GetString ("_Reset");
			w7.UseUnderline = true;
			w4.Add (w7);
			w3.Add (w4);
			this.clear_background.Add (w3);
			this.hbox4.Add (this.clear_background);
			global::Gtk.Box.BoxChild w11 = ((global::Gtk.Box.BoxChild)(this.hbox4 [this.clear_background]));
			w11.Position = 2;
			w11.Expand = false;
			w11.Fill = false;
			this.vbox4.Add (this.hbox4);
			global::Gtk.Box.BoxChild w12 = ((global::Gtk.Box.BoxChild)(this.vbox4 [this.hbox4]));
			w12.Position = 0;
			w12.Expand = false;
			w12.Fill = false;
			// Container child vbox4.Gtk.Box+BoxChild
			this.shadow_check = new global::Gtk.CheckButton ();
			this.shadow_check.CanFocus = true;
			this.shadow_check.Name = "shadow_check";
			this.shadow_check.Label = global::Mono.Unix.Catalog.GetString ("Show window shadow");
			this.shadow_check.DrawIndicator = true;
			this.shadow_check.UseUnderline = true;
			this.vbox4.Add (this.shadow_check);
			global::Gtk.Box.BoxChild w13 = ((global::Gtk.Box.BoxChild)(this.vbox4 [this.shadow_check]));
			w13.Position = 1;
			w13.Expand = false;
			w13.Fill = false;
			// Container child vbox4.Gtk.Box+BoxChild
			this.animation_check = new global::Gtk.CheckButton ();
			this.animation_check.CanFocus = true;
			this.animation_check.Name = "animation_check";
			this.animation_check.Label = global::Mono.Unix.Catalog.GetString ("Animate window");
			this.animation_check.Active = true;
			this.animation_check.DrawIndicator = true;
			this.animation_check.UseUnderline = true;
			this.vbox4.Add (this.animation_check);
			global::Gtk.Box.BoxChild w14 = ((global::Gtk.Box.BoxChild)(this.vbox4 [this.animation_check]));
			w14.Position = 2;
			w14.Expand = false;
			w14.Fill = false;
			this.Add (this.vbox4);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.Hide ();
			this.background_colorbutton.ColorSet += new global::System.EventHandler (this.OnBackgroundColorbuttonColorSet);
			this.clear_background.Clicked += new global::System.EventHandler (this.OnClearBackgroundClicked);
			this.shadow_check.Toggled += new global::System.EventHandler (this.OnShadowCheckToggled);
			this.animation_check.Toggled += new global::System.EventHandler (this.OnAnimationCheckToggled);
		}
	}
}
